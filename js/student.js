function fullStudent()
{
    var k = parseFloat(document.getElementById("k").value);

    tmpDist = studentDistribution(k);
    distribution = combine(tmpDist[0], tmpDist[1]);

    tmp = studentDencity(k);
    dencity = combine(tmp[0], tmp[1]);

    var allData = [
        {
            label: "Student Distribution",
            color: 1,
            data: distribution
        },
        {
            label: "Student Dencity",
            color: 2,
            data: dencity
        }
    ];

    var plotConf = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            }
        },
        xaxis: {
            min: -10,
            max: 10
        }
    };

    drawAll(allData, plotConf);
}

function studentDistribution(k)
{
    var stud_seq = get_student_sequence(k, 5000);
    var chi_seq = norm_sequence(1000, -4, 4);
    var stud_pro = students_distribution(k, chi_seq);

    return [chi_seq, stud_pro];
}

function studentDencity(k)
{
    var stud_seq = get_student_sequence(k, 5000);
    var stud_dencity = dencity_student_sequence(k, stud_seq);

    console.log(variance(stud_seq, stud_dencity)+ "\n" + varience(stud_seq));

    return [stud_seq, stud_dencity];
}

function get_student_sequence(n, size)
{
    var result = [];
    for(var i = 0; i < size; i++){
        var value = 0;
        for(var j = 0; j < n; j++){
            value += Math.pow(_randnormal(-1, 0, 1, 1), 2);
        }
        result[i] = _randnormal(-1, 0, 1, 1)/(value/n);
    }
    return result.sort((a,b)=>a-b);
}

function _student_dencity_value(n, x)
{
    return gamma_full((n+1)/2)/(Math.sqrt(n*Math.PI)*gamma_full(n/2)*Math.pow(1+x*x/n, (n+1)/2));
}

function dencity_student_sequence(n, sequence)
{
    var result = [];
    for(var i = 0; i < sequence.length; i++){
        result[i] = _student_dencity_value(n, sequence[i]);
    }
    return result;
}

function students_distribution(n, sequence)
{
    var prob = [];
    var g_n1 = gamma_full((n + 1) / 2);
    var g_n = gamma_full(n / 2);
    for(var c=0; c < sequence.length; c++){
        var hgf = hypergeometric(0.5, (n + 1) / 2, 1.5, - Math.pow(sequence[c], 2) / n, n);
        prob[c] = 0.5 + sequence[c] * g_n1 * hgf / (Math.sqrt(Math.PI*n)*g_n);
    }
    return prob;
}

function hypergeometric(a, b, c, z, k=0)
{
    var err = 0;
    if(k > 0) err = k < 5 ? Math.pow(k, 2) * 0.01 : k < 8 ? 0.2 : 0.25;
    if (Math.abs(z) > 1 || 1 - Math.abs(z) <err) {
    //if (Math.abs(z) > 1) {
        return Math.pow(1 - z, -b) * hypergeometric(c-a, b, c, z / (z - 1), k);
    }
    if (b == 0 && c == 0) return 1;
    if (b == 0) return 1;
    if (c == 0) {
        return Math.pow(z, 1 - c) * hypergeometric(b - c + 1, a - c + 1, 2 - c, z, k);
    }
    var result = 0;
    if (a == b && b == c) {
            var factorial = 1;
            for (let k = 0; k <= 50; k++) {
                if(k > 0) factorial*=k;
                let ak = gamma_full(a + k) / gamma_full(a);
                if (!isFinite(gamma_full(a))) {
                    if (k != 0) {
                        ak = 0;
                    }
                    else {
                        ak = 1;
                    }
                }
                result += (ak * Math.pow(z, k) / factorial);
            }
    }
    else {
        var factorial = 1;
        for (let k = 0; k < 50; k++) {
            if(k > 0) factorial*=k;
            let ak = gamma_full(a + k) / gamma_full(a);
            if (!isFinite(gamma_full(a))) {
                if (k != 0) {
                    ak = 0;
                }
                else {
                    ak = 1;
                }
            }
            let bk = gamma_full(b + k) / gamma_full(b);
            if (!isFinite(gamma_full(b))) {
                if (b+k>0) {
                    bk = 0;
                }
                else {
                    bk = 1;
                }
            }
            let ck = gamma_full(c + k) / gamma_full(c);
            if (!isFinite(gamma_full(c))) {
                if (k != 0) {
                    ck = 0;
                }
                else {
                    ck = 1;
                }
            }
            let tempro = ((ak * bk * (Math.pow(z, k))) / (ck * factorial));
            result += tempro;
        }
    }
    return result;
}

function gamma_full(z)
{
    if((z>0)&&(z<1.0)) 
       return gamma_full(z+1)/z;	   	// рекурентное соотношение для 0
    if(z > 2 ) 
        return (z-1)*gamma_full(z-1);	 	// рекурентное соотношение для z>2 
    if(z<=0) 
       return Math.PI/(Math.sin(Math.PI*z)*gamma_full(1-z));	// рекурентное соотношение для z<=0 
    return gammaapprox(z);	// 1<=z<=2 использовать аппроксимацию
}

function gammaapprox(x)
{
    var p=[-1.71618513886549492533811e+0,
                    2.47656508055759199108314e+1,
                   -3.79804256470945635097577e+2,
                    6.29331155312818442661052e+2,
                    8.66966202790413211295064e+2,
                   -3.14512729688483675254357e+4,
                   -3.61444134186911729807069e+4,
                    6.64561438202405440627855e+4];

    var q=[-3.08402300119738975254353e+1,
                    3.15350626979604161529144e+2,
                   -1.01515636749021914166146e+3, 
                   -3.10777167157231109440444e+3,
                    2.25381184209801510330112e+4,
                    4.75584627752788110767815e+3,
                   -1.34659959864969306392456e+5,
                   -1.15132259675553483497211e+5];
    var z1 = x - 1.0;
    var a = 0.0;
    var b = 1.0;
    for(k1 = 0; k1<8; k1++){
            a =(a+p[k1])*z1;
            b = b*z1+q[k1];
    }
    return (a/b+1.0);
}

function _randnormal(minimum, mean, maximum, stdev) 
{
  // start with two uniformly distributed random numbers
  var rn1 = Math.random();
  var rn2 = Math.random();
   
  // Box-Muller transform
  var r = Math.sqrt(-2 * Math.log(rn1));
  var theta = 2 * Math.PI * rn2;
  var x = r * Math.cos(theta); // 1st of 2 random numbers generated
   
  // transform 1st Box-Muller output to desired value space
  var n = x * stdev + mean;
   
  // if value within range, return result
  if (n >= minimum & n <= maximum)
    return n;
   
  // 1st random number wasn't in range, so try 2nd random number
  var y = r * Math.sin(theta);
  n = y * stdev + mean;
   
  // if still not in range, start over with another random pair
  if (n < minimum | n > maximum)
    return _randnormal(minimum, mean, maximum, stdev); // try again
   
  // 2nd value is in range, return results
  return n;
}

function norm_sequence(size, mu, sigma)
{
    var list = [];
    for(var i = 0; i < size; i++)
    {
        list[list.length] = _randnormal(mu-3*sigma, 0, mu+3*sigma, 1);
    }
    list.sort((a, b)=>a-b);
    return list;
}
