function fullNormal()
{
    var mu = parseFloat(document.getElementById("mu").value);
    var chi = parseFloat(document.getElementById("chi").value);

    tmpSeq = getRandomSequence(-4, 4, 1000);
    tmpDist = normalDistribution(mu, chi, tmpSeq);

    distribution = combine(tmpSeq, tmpDist);
    tmp = normalDencity(mu, chi);
    dencity = combine(tmp[0], tmp[1]);

    var allData = [
        {
            label: "Normal Distribution",
            color: 1,
            data: distribution
        },
        {
            label: "Normal Dencity",
            color: 2,
            data: dencity
        }
    ];

    var plotConf = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            }
        }
    };

    drawAll(allData, plotConf);    
}

function normalDistribution(m, chi, sequence)
{
    var distribution = [];

    for(var i=0; i < sequence.length; i++)
    {
        var erfArgs = (sequence[i] - m) / Math.sqrt(2 * chi * chi);
        var temp = 0.5 + erf(erfArgs) / 2;

        distribution[i] = temp;
    }

    return distribution;
}

function normalDencity(mu, chi)
{
    var norm_seq = normSequence(5000, mu, chi);
    var gauss_dencity = dencityNormSequence(0, chi, norm_seq);

    console.log("" + variance(norm_seq, gauss_dencity) + "\n" + varience(norm_seq));

    return [norm_seq, gauss_dencity];
}

function normSequence(size, mu, sigma)
{
    var list = [];
    for(var i = 0; i < size; i++)
    {
        list[list.length] = _randnormal(mu - 3 * sigma, 0, mu + 3 * sigma, 1);
    }

    list.sort((a, b)=>a-b);

    return list;
}

function dencityNormSequence(mu, sigma, seq)
{
    result = [];
    for(var i = 0; i < seq.length; i++)
    {
        result[i] = _norm_dencity_value(mu, sigma, seq[i]);
    }
    return result;
}

function erf(x)
{
    var result = 0;
    var factorial = 1;

    for(var j=0; j < 40; j++)
    {
        if(j > 0) 
        {
            factorial *= j;
        }
        result += 2 * Math.pow(-1, j) * Math.pow(x, 2 * j + 1) / (Math.sqrt(Math.PI) * factorial * (2 * j+1));
    }

    return result;
}

function _norm_dencity_value(mu, sigma, x)
{
    return Math.exp(-(x - mu)*(x - mu)/(2*sigma*sigma));
}

function variance(seq_value, seq_pro)
{
    var result = 0;

    for(var i=0; i < seq_pro.length; i++)
    {
        result += seq_pro[i]*seq_value[i];
    }

    return result / seq_pro.length;
}

function varience(data)
{
    var sqr_sum = 0, sum = 0;

    for(var i=0; i < data.length; i++)
    {
        sqr_sum += data[i]*data[i];
        sum += data[i];
    }

    var result = (sqr_sum - sum*sum/data.length)/(data.length-1);;
    return result;
}