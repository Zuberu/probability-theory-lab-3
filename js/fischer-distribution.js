function fullFischer()
{
    var d1 = parseFloat(document.getElementById("d1").value);
    var d2 = parseFloat(document.getElementById("d2").value);

    tmpDist = fischerDistribution(d1, d2);
    distribution = combine(tmpDist[0], tmpDist[1]);

    tmp = fischerDencity(d1, d2);
    dencity = combine(tmp[0], tmp[1]);

    var allData = [
        {
            label: "Fischer Distribution",
            color: 1,
            data: distribution
        },
        {
            label: "Fischer Dencity",
            color: 2,
            data: dencity
        }
    ];

    var plotConf = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            }
        },
        xaxis: {
            max: 19
        },
        yaxis: {
            max: 1
        }
    };

    drawAll(allData, plotConf);
}

function fischerDistribution(d1, d2)
{
    var chi_seq = get_fischer_sequence(1000, d1, d2);
    var chi_pro = fischer_distribution(d1, d2, chi_seq);

    return [chi_seq, chi_pro];
}

function fischerDencity(d1, d2)
{
    var chi_seq = get_fischer_sequence(10000, d1, d2);
    var p = dencity_fischer_sequence(d1, d2, chi_seq)

    console.log(variance(chi_seq, p)+ "\n" + varience(chi_seq));

    return [chi_seq, p];
}

function dencity_fischer_sequence(d1, d2, sequence)
{
    var result = [];

    for(var i = 0; i < sequence.length; i++)
    {
        result[i] = _fischer_dencity_value(d1, d2, sequence[i]);
    }

    return result;
}

function _fischer_dencity_value(d1, d2, x)
{
    return Math.sqrt(
        Math.pow(d1 * x, d1) * Math.pow(d2, d2) /
        Math.pow(d1 * x + d2, d1 + d2)) /
        (x * beta_complete(d1 / 2, d2 / 2)
    );
}

function get_fischer_sequence(size, k1, k2)
{
    var list = [];

    for(var i = 0; i < size; i++)
    {
        list[list.length] = (_chi_value(k1) / k1) / (_chi_value(k2) / k2);
    }

    list.sort((a, b)=>a-b);
    return list;
}

function fischer_distribution(d1, d2,sequence)
{
    var result = [];

    for(var i = 0; i < sequence.length; i++)
    {
        var x = sequence[i];
        result[i] = beta_regularized_incomplete(d1*x/(d1*x+d2),d1/2, d2/2);
    }

    return result;
}

function beta_regularized_incomplete(x, a, b)
{
    return beta_incomplete(x, a, b) / beta_complete(a, b);
}

function beta_complete(x, y)
{
    var gfx = gamma_full(x);
    var gfy = gamma_full(y);
    var gfxy = gamma_full(x + y);

    return gfx * gfy / gfxy;
}

function beta_incomplete(x, a, b)
{
    return Math.pow(x, a) * hypergeometric(a, 1 - b, a + 1, x) / a;
}