function fullChi()
{
    var k = parseFloat(document.getElementById("k").value);

    tmpDist = chiDistribution(k);
    distribution = combine(tmpDist[0], tmpDist[1]);

    tmp = chiDencity(k);
    dencity = combine(tmp[0], tmp[1]);

    var allData = [
        {
            label: "Chi Distribution",
            color: 1,
            data: distribution
        },
        {
            label: "Chi Dencity",
            color: 2,
            data: dencity
        }
    ];

    var plotConf = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            }
        },
        xaxis: {
            min: 0,
            max: 10
        },
        yaxis: {
            min: 0,
            max: 1.2
        }
    };

    drawAll(allData, plotConf);
}

function chiDistribution(k)
{
    var chi_seq = get_chi_sequence(1000, k);
    var chi_pro = chiSquaredDistribution(k, chi_seq);

    return [chi_seq, chi_pro];
}

function chiDencity(k)
{
    var chi_seq = get_chi_sequence(1000, k);
    var denc_chi_seq = dencity_chi_squared_sequence(k, chi_seq);

    console.log(variance(chi_seq, denc_chi_seq)+ "\n" + varience(chi_seq));

    return [chi_seq, denc_chi_seq];
}

function get_chi_sequence(size, k)
{
    var list = [];

    for(var i = 0; i < size; i++)
    {
        list[i] = _chi_value(k);
    }

    list.sort((a, b)=>a-b);
    return list;
}

function chiSquaredDistribution(k, sequence)
{
    var result = [];
    var gamma_k = gamma_full(k / 2);

    for(i = 0; i < sequence.length; i++)
    {
        result[i] = gamma_inc(k / 2, sequence[i] / 2) / gamma_k;
    }

    return result;
}

function _chi_value(k)
{
    var temp = 0;

    for(var j = 0; j < k; j++)
    {
        temp += Math.pow(_randnormal(0, 0, 3, 1), 2);
    }

    return temp;
}

function gamma_inc(z, x)
{
    let res = 0;
    let tmp = 1;

    for (let k = 0; k < 50; k++)
    {
        tmp *= (z + k);
        res += (Math.pow(x, z) * Math.exp(-x) * Math.pow(x, k)) / (tmp);
    }

    return res;
}

function dencity_chi_squared_sequence(k, sequence)
{
    var result = [];

    for(var i = 0; i < sequence.length; i++)
    {
        result[i] = _chi_dencity_value(k, sequence[i]);
    }

    return result;
}

function _chi_dencity_value(k, x)
{
    return Math.pow(0.5, k / 2) * Math.pow(x, k / 2 - 1) * Math.exp(-x / 2) / gamma_full(k / 2);
}