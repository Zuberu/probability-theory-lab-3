var aR = document.getElementById("a-row");
var bR = document.getElementById("b-row");
var lambdaR = document.getElementById("lambda-row");
var muR = document.getElementById("mu-row");
var chiR = document.getElementById("chi-row");
var kR = document.getElementById("k-row");
var d1R = document.getElementById("d1-row");
var d2R = document.getElementById("d2-row");

var current = "uniform";

$("#unifo").click(function() {
    lambdaR.style.display = "none";
    muR.style.display = "none";
    chiR.style.display = "none";
    kR.style.display = "none";
    d1R.style.display = "none";
    d2R.style.display = "none";

    aR.style.display = "table-row";
    bR.style.display = "table-row";

    current = "uniform";
});

$("#expo").click(function() {
    aR.style.display = "none";
    bR.style.display = "none";
    muR.style.display = "none";
    chiR.style.display = "none";
    kR.style.display = "none";
    d1R.style.display = "none";
    d2R.style.display = "none";

    lambdaR.style.display = "table-row";

    current = "exponential";
});

$("#norm").click(function() {
    aR.style.display = "none";
    bR.style.display = "none";
    lambdaR.style.display = "none";
    kR.style.display = "none";
    d1R.style.display = "none";
    d2R.style.display = "none";

    muR.style.display = "table-row";
    chiR.style.display = "table-row";

    current = "normal";
});

$("#stu").click(function() {
    lambdaR.style.display = "none";
    aR.style.display = "none";
    bR.style.display = "none";
    muR.style.display = "none";
    chiR.style.display = "none";
    d1R.style.display = "none";
    d2R.style.display = "none";

    kR.style.display = "table-row";

    current = "student";
});

$("#chi-d").click(function() {
    lambdaR.style.display = "none";
    aR.style.display = "none";
    bR.style.display = "none";
    muR.style.display = "none";
    chiR.style.display = "none";
    d1R.style.display = "none";
    d2R.style.display = "none";

    kR.style.display = "table-row";

    current = "chi-squared";
});

$("#fis").click(function() {
    lambdaR.style.display = "none";
    aR.style.display = "none";
    bR.style.display = "none";
    muR.style.display = "none";
    chiR.style.display = "none";
    kR.style.display = "none";

    d1R.style.display = "table-row";
    d2R.style.display = "table-row";

    current = "fischer";
});


$("#count-btn").click(function() {

    var a = parseFloat(document.getElementById("a").value);
    var b = parseFloat(document.getElementById("b").value);
    var lambda = parseFloat(document.getElementById("lambda").value);
    var mu = parseFloat(document.getElementById("mu").value);
    var chi = parseFloat(document.getElementById("chi").value);

    var tmpSeq;;
    var tmpDist;
    var result = [];

    switch(current)
    {
        case "uniform":
        {
            fullUniform();
            break;
        }
        case "exponential":
        {
            fullExponential();
            break;
        }
        case "normal":
        {
            fullNormal();
            break;
        }
        case "student":
        {
            fullStudent();
            break;
        }
        case "chi-squared":
        {
            fullChi();
            break;
        }
        case "fischer":
        {
            fullFischer();
            break;
        }
        default:
        {
            alert("Error!");
        }
    }
});


function combine(x, y)
{
    if (x.length != y.length)
    {
        return [];
    }

    var resultList = [];

    for(var i=0; i < x.length; i++)
    {
        resultList.push([x[i], y[i]]);
    }

    return resultList;
}

function draw(data, name, color)
{
    var allData = [
        {
            label: name,
            color: color,
            data: data
        }
    ];
    var plotConf = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            }
        }
    };

    $.plot($("#graph"), allData, plotConf);
}

function drawAll(allData, plotConf)
{
    $.plot($("#graph"), allData, plotConf);
}

draw();

lambdaR.style.display = "none";
muR.style.display = "none";
chiR.style.display = "none";
kR.style.display = "none";
d1R.style.display = "none";
d2R.style.display = "none";

aR.style.display = "table-row";
bR.style.display = "table-row";

current = "uniform";

document.getElementById("unifo").checked = true;