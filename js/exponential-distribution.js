function fullExponential()
{
    var lambda = parseFloat(document.getElementById("lambda").value);

    tmpSeq = getRandomSequence(0, 10, 1000);
    tmpDist = exponentialDistribution(0, 10, tmpSeq, lambda);

    distribution = combine(tmpSeq, tmpDist);
    tmp = exponentialDencity(lambda);
    dencity = combine(tmp[0], tmp[1]);

    var allData = [
        {
            label: "Exp Distribution",
            color: 1,
            data: distribution
        },
        {
            label: "Exp Dencity",
            color: 2,
            data: dencity
        }
    ];

    var plotConf = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            }
        }
    };

    drawAll(allData, plotConf);
}

function exponentialDistribution(a, b, sequence, lambda=0.5)
{
    var distribution = [];
    
    for(var i=0; i < sequence.length; i++)
    {
        distribution.push(1 - Math.exp(-lambda * sequence[i]));
    }

    return distribution;
}

function exponentialDencity(lambda)
{
    var exp_seq = get_exp_sequence(5000, 0, 5, lambda);
    var exp_dencity = dencity_exp_sequence(lambda, exp_seq);

    console.log(variance(exp_seq, exp_dencity)+ "\n" + varience(exp_seq));

    return [exp_seq, exp_dencity];
}

function get_exp_sequence(size, min, max, lambda)
{
    var sequence = [];

    for(i=0; i < size; i++)
    {
        sequence[i] = - Math.log(Math.random()) / lambda;
    }

    return sequence.sort((a, b)=>a-b);
}

function dencity_exp_sequence(lambda, seq)
{
    result = [];

    for(var i = 0; i < seq.length; i++)
    {
        result[i] = _exp_dencity_value(lambda, seq[i]);
    }

    return result;
}

function _exp_dencity_value(lambda, x)
{
    return lambda * Math.exp(-lambda * x);
}
