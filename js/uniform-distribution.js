function fullUniform()
{
    var a = parseFloat(document.getElementById("a").value);
    var b = parseFloat(document.getElementById("b").value);

    tmpSeq = getRandomSequence(a, b, 1000);
    tmpDist = uniformDistribution(a, b, tmpSeq);

    distribution = combine(tmpSeq, tmpDist);
    tmp = uniformDencity(a, b);
    dencity = combine(tmp[0], tmp[1]);

    var allData = [
        {
            label: "Uniform Distribution",
            color: 1,
            data: distribution
        },
        {
            label: "Uniform Dencity",
            color: 2,
            data: dencity
        }
    ];

    var plotConf = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            }
        }
    };

    drawAll(allData, plotConf);   
}

function uniformDistribution(a, b, sequence)
{
    var distribution = [];

    for(var i=0; i < sequence.length; i++)
    {
        var temp = (sequence[i] - a) / (b - a)
        distribution.push(temp);
    }

    return distribution;
}

function uniformDencity(a, b)
{
    var seq = get_random_sequence(a, b);
    var equal_den = (decity_equal_sequence(a, b, seq));

    console.log(variance(seq, equal_den)+ "\n" + varience(seq));

    return [seq, equal_den];
}

function decity_equal_sequence(a, b, seq)
{
    var result = [];

    for(var i = 0; i < seq.length; i++)
    {
        result[i] = _equal_dencity_value(a, b, seq[i]);
    }

    return result;
}

function _equal_dencity_value(a, b, x)
{
    var result = 0;

    if(x >= a && x <= b)
    {
        result = 1 / (b - a);
    }

    return result;
}

function get_random_sequence(min, max, size=200)
{
    var sequence = [];

    for(i=0; i < size; i++)
    {
        sequence[i] = Math.random() * (max - min) + min;
    }

    return sequence.sort((a, b)=>a-b);
}
