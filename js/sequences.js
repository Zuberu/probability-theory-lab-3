function getRandomSequence(min, max, size=2000)
{
    var sequence = [];

    for(var i=0; i < size; i++)
    {
        sequence[i] = Math.random() * (max - min) + min;
    }

    sequence.sort((a, b) => a - b);

    return sequence;
}

function normalSequence(size, min, max)
{
    var list = [];

    for(var i = 0; i < size; i++)
    {
        list.push(randnormal(0, 0, 1, 1));
    }

    list.sort((a, b) => a - b);

    return list;
}